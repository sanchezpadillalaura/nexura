<?php 
include '../conexion.php';
include '../template.php';
include '../class/roles.php';

if (isset($_GET['case'])) {
	$case = $_GET['case'];
}else if (isset($_POST["case"])) {
	$case = $_POST["case"];
}

switch ($case) {
	case 'crearEditarRol':
		$roles =  new Roles();
		if(is_numeric($_POST['id']) && $_POST['id']>0){
			$rol = $roles->editar($_POST);
			$type='2';
		}else{
			$rol = $roles->crear($_POST);
			$type='1';
		}

		if($rol['rps']){
			$rps = json_encode(array("rps" => 1, 'type'=>$type, 'msj' => 'Información guardada correctamente'));
		}else{
			$rps = json_encode(array("rps" => 0, 'type'=>$type, "msj" => 'Ocurrio un error inesperado, por favor contacte  a soporte' ));
		}
		break;

	case 'Eliminar':
		$roles =  new Roles();
		if(is_numeric($_POST['id']) && $_POST['id']>0){
			$rol = $roles->eliminar($_POST);
			if($rol['rps']){
				$rps = json_encode(array("rps" => 1, 'msj' => 'Rol eliminado correctament'));
			}else{
				$rps = json_encode(array("rps" => 0, "msj" => 'Ocurrio un error inesperado, por favor contacte  a soporte' ));
			}
		}else{
			$rps = json_encode(array("rps" => 0, "msj" => 'Rol incorrecto' ));
		}
		break;
}

echo $rps;

?>
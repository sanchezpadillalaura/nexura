<?php 
include '../conexion.php';
include '../template.php';
include '../class/areas.php';

if (isset($_GET['case'])) {
	$case = $_GET['case'];
}else if (isset($_POST["case"])) {
	$case = $_POST["case"];
}

switch ($case) {
	case 'crearEditarArea':
		$areas =  new Areas();
		if(is_numeric($_POST['id']) && $_POST['id']>0){
			$rol = $areas->editar($_POST);
			$type='2';
		}else{
			$rol = $areas->crear($_POST);
			$type='1';
		}

		if($rol['rps']){
			$rps = json_encode(array("rps" => 1, 'type'=>$type, 'msj' => 'Información guardada correctamente'));
		}else{
			$rps = json_encode(array("rps" => 0, 'type'=>$type, "msj" => 'Ocurrio un error inesperado, por favor contacte  a soporte' ));
		}
		break;

	case 'Eliminar':
		$areas =  new Areas();
		if(is_numeric($_POST['id']) && $_POST['id']>0){
			$rol = $areas->eliminar($_POST);
			if($rol['rps']){
				$rps = json_encode(array("rps" => 1, 'msj' => 'Area eliminada correctament'));
			}else{
				$rps = json_encode(array("rps" => 0, "msj" => 'Ocurrio un error inesperado, por favor contacte  a soporte' ));
			}
		}else{
			$rps = json_encode(array("rps" => 0, "msj" => 'Area incorrecto' ));
		}
		break;
}

echo $rps;

?>
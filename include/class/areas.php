<?php
class Areas
{
	private $conexiondb;
	public $id;
	public $nombre;

	function __construct()
	{
		try
		{
			$BD = new BD();
			$this->conexiondb = $BD->conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function listar(){
		
		try
		{
			$sql = "SELECT id, nombre FROM areas ORDER BY nombre ASC";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->execute();
			$result = $sentencia->get_result();
			$result->fetch_all(MYSQLI_ASSOC);
			$this->conexiondb->close();
			return $result;

		} catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function crear($POST)
	{
		try {
			$nombre = trim($this->conexiondb->real_escape_string($POST["nombre"]));

			$sql = "INSERT INTO `areas` ( nombre) VALUES (?)";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('s', $nombre);
			if($sentencia->execute()){
				$filas 	= $sentencia->fetch();
				$result = array('rps'=>true, 'id'=>$sentencia->insert_id);
			}else{
				$result = array('rps'=>false);
			}
			$this->conexiondb->close();
			return $result;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function detalle($id)
	{
		try {
			$id = trim($this->conexiondb->real_escape_string($id));
			$sql = "SELECT id, nombre FROM areas WHERE id = ?";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('i', $id);
			$sentencia->execute();
			$sentencia->bind_result($id, $nombre);

			$sentencia->store_result();
			if($sentencia->num_rows>0){
				$filas 	= $sentencia->fetch();
				$result = array('existe' => true, 'id' => $id, 'nombre' => $nombre);
			}else{
				$result = array('existe' => false);
			}
			$this->conexiondb->close();
			return $result;
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function editar($POST)
	{
		try {
			$nombre = trim($this->conexiondb->real_escape_string($POST["nombre"]));
			$id 	= trim($this->conexiondb->real_escape_string($POST["id"]));

			$sql = "UPDATE `areas` SET nombre=? WHERE id=?";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('si', $nombre, $id);
			if($sentencia->execute()){
				$filas 	= $sentencia->fetch();
				$result = array('rps'=>true);
			}else{
				$result = array('rps'=>false);
			}
			$this->conexiondb->close();
			return $result;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function eliminar($POST)
	{
		try {
			$id 	= trim($this->conexiondb->real_escape_string($POST["id"]));

			$sql = "DELETE FROM `areas` WHERE id=?";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('i', $id);
			if($sentencia->execute()){
				$filas 	= $sentencia->fetch();
				$result = array('rps'=>true);
			}else{
				$result = array('rps'=>false);
			}
			$this->conexiondb->close();
			return $result;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

}

?>
<?php
class Empleados
{
	private $conexiondb;
	public $id;
	public $nombre;
	public $email;
	public $sexo;
	public $boletin;
	public $area_id;

	function __construct()
	{
		try
		{
			$BD = new BD();
			$this->conexiondb = $BD->conectar();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function listar(){
		
		try
		{
			$sql = "SELECT id, nombre, email, sexo, boletin, area_id, (SELECT nombre FROM areas a WHERE a.id= area_id) area FROM empleados ORDER BY nombre ASC";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->execute();
			$result = $sentencia->get_result();
			$result->fetch_all(MYSQLI_ASSOC);
			$this->conexiondb->close();
			return $result;

		} catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function crear($POST)
	{
		try {
			$nombre = trim($this->conexiondb->real_escape_string($POST["nombre"]));

			$sql = "INSERT INTO `empleados` ( nombre) VALUES (?)";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('s', $nombre);
			if($sentencia->execute()){
				$filas 	= $sentencia->fetch();
				$result = array('rps'=>true, 'id'=>$sentencia->insert_id);
			}else{
				$result = array('rps'=>false);
			}
			$this->conexiondb->close();
			return $result;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function detalle($id)
	{
		try {
			$id = trim($this->conexiondb->real_escape_string($id));
			$sql = "SELECT id, nombre, email, sexo, boletin, area_id, descripcion FROM empleados WHERE id = ?";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('i', $id);
			$sentencia->execute();
			$sentencia->bind_result($id, $nombre, $email, $sexo, $boletin, $area_id, $descripcion);

			$sentencia->store_result();
			if($sentencia->num_rows>0){
				$filas 	= $sentencia->fetch();
				$result = array('existe' => true, 'id' => $id, 'nombre' => $nombre, 'email' => $email, 'sexo' => $sexo, 'boletin' => $boletin, 'area_id' => $area_id, 'descripcion'=>$descripcion);
			}else{
				$result = array('existe' => false);
			}
			$this->conexiondb->close();
			return $result;
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function editar($POST)
	{
		try {
			$nombre = trim($this->conexiondb->real_escape_string($POST["nombre"]));
			$id 	= trim($this->conexiondb->real_escape_string($POST["id"]));
			$email 	= trim($this->conexiondb->real_escape_string($POST["email"]));
			$sexo 	= trim($this->conexiondb->real_escape_string($POST["sexo"]));
			$boletin 	= ((isset($POST["boletin"]) && is_numeric($POST["boletin"]))?$POST["boletin"]:'0');
			$area_id 	= trim($this->conexiondb->real_escape_string($POST["area_id"]));
			$descripcion 	= trim($this->conexiondb->real_escape_string($POST["descripcion"]));

			$sql = "UPDATE `empleados` SET nombre=?,email=?,sexo=?,boletin=?,area_id=?,descripcion=? WHERE id=?";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('ssssisi', $nombre, $email, $sexo, $boletin, $area_id, $descripcion, $id);
			if($sentencia->execute()){
				$filas 	= $sentencia->fetch();
				$result = array('rps'=>true);
			}else{
				$result = array('rps'=>false);
			}
			$this->conexiondb->close();
			return $result;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function eliminar($POST)
	{
		try {
			$id 	= trim($this->conexiondb->real_escape_string($POST["id"]));

			$sql = "DELETE FROM `empleados` WHERE id=?";
			$sentencia = $this->conexiondb->prepare($sql);
			$sentencia->bind_param('i', $id);
			if($sentencia->execute()){
				$filas 	= $sentencia->fetch();
				$result = array('rps'=>true);
			}else{
				$result = array('rps'=>false);
			}
			$this->conexiondb->close();
			return $result;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

}

?>
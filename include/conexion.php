<?php
class BD
{
	public $host;
	private $database;
	private $user;
	private $password;
	private $conexion;
	private $port;

	function __construct()
	{
		$this->host 	= 'localhost';
		$this->database = 'nexura';
		$this->user 	= 'root';
		$this->password = '';
		$this->conexion = null;
		$this->port = 3306;
	}

	public function conectar(){
		
		$this->conexion = new mysqli($this->host, $this->user, $this->password, $this->database, $this->port);
		if (mysqli_connect_errno()) {
		    printf("Connect failed: %s\n", mysqli_connect_error());
		    exit();
		}

		$this->conexion->set_charset("utf8");

		return $this->conexion;
	}

	public function cerrar(){
		$this->conexion->close();
	}
}

?>
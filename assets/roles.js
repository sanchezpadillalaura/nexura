$(document).ready(function(){
   $("form[name=formRol]").submit(function() {
       //Desactivamos el boton
       $("#BtnEnviar").attr('disabled', 'disabled')
       var formData = new FormData(document.getElementById("formRol"));
       $.ajax({
          type: "POST",
          dataType: "json",
          url: "../include/componentes/rol.php", 
          data: $("form[name=formRol]").serialize(),
          success: function(result){
             if(result.rps){
               swal("Bien", result.msj, "success");
               $("#BtnEnviar").removeAttr('disabled')
               if(result.type=='1'){
                  document.getElementById('formRol').reset();
                  $('#nombre').focus();
               }
             }else{
               swal("oops!", result.msj, "error");
               $("#BtnEnviar").removeAttr('disabled')
             }

          },error: function(XMLHttpRequest, textStatus, errorThrown){ 
             console.log('Algo salio mal')
          }
       });
       return false;
   });

   $('.btnEliminar').on('click', function (e) {
      let nombre = $(this).data('nombre')
      let id = $(this).data('id')
      swal({
         title: "¿Estas Seguro?",
         text: "Se eliminara el rol *"+nombre+"*, Esta acción es irreversible.",
         icon: "warning",
         buttons: {
             cancel: "Cancelar",
             catch: {
                 text: "Si, Eliminar",
                 closeModal: false,
                 className: 'swal-button--danger'
             },
         },
         })
        .then((willDelete) => {
            if (willDelete) {
               $.ajax({
                  type: "POST",
                  dataType: "json",
                  url: "../include/componentes/rol.php", 
                  data: {'case':'Eliminar', 'id': id},
                  success: function(result){
                     if(result.rps){
                        swal("Bien", result.msj, "success");
                        $('.tr'+id).remove()
                     }else{
                        swal("oops!", result.msj, "error");
                     }

                  },error: function(XMLHttpRequest, textStatus, errorThrown){ 
                      console.log('Algo salio mal')
                  }
               });

               return false;
            }
        });
   })
})
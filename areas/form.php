<?php
include '../include/conexion.php';
include '../include/template.php';
include '../include/class/areas.php';
$template =  new Template('Crear Area');

if(isset($_GET['id']) && is_numeric($_GET['id'])){
  $areas =  new Areas();
  $id_area = $_GET['id'];
  $area = $areas->detalle($id_area);
  $titulo = "Editar Area";
}else{
  $titulo = "Crear Area";
  $id_area = 0;
}

?>
<?php echo $template->header() ?>
  <h1><?php echo $titulo ?></h1>
  <div class="row">
    <div class="col-md-12">
      <a href="index.php" class="btn btn-sm btn-primary float-end" ><i class="fas fa-undo"></i> Volver al Listado</a>
    </div>
    <div class="col-md-12">
      <form name="formArea" id="formArea">
        <div class="mb-3 row">
          <label for="nombre" class="col-sm-3 col-form-label fw-bold text-end">Nombre *</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" value="<?php echo ((isset($area['nombre']))?$area['nombre']:'') ?>" name="nombre" maxlength='255' id="nombre" required placeholder="Nombre del area">
          </div>
        </div>
       
        <div class="mb-3 row">
          <div class="col-sm-9 offset-sm-3">
            <input type="hidden" name="case" value="crearEditarArea">
            <input type="hidden" name="id" value="<?php echo $id_area ?>">
            <button class="btn btn-primary" id="BtnEnviar">Guardar</button>
          </div>
        </div>
    </form>
    </div>
  </div>
<?php 
$script ='<script type="text/javascript" src="../assets/areas.js"></script>';
echo $template->footer($script) ?>
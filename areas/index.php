<?php
include '../include/conexion.php';
include '../include/template.php';
include '../include/class/areas.php';
$template =  new Template('Listado de Areas');
$areas =  new Areas();
$listadoAreas = $areas->listar();

?>

<?php echo $template->header() ?>
  <h1>Lista de areas</h1>
  <div class="row">
    <div class="col-md-12">
      <a href="form.php" class="btn btn-sm btn-primary float-end"><i class="fas fa-user-plus"></i> Crear</a>
    </div>
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th><i class="fas fa-tags"></i> Nombre</th>
              <th>Modificar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoAreas  as $key => $area): ?>
              <tr class="tr<?php echo $area['id']?>">
                <td><?php echo $area['nombre'] ?></td>
                <td><a href="form.php?id=<?php echo $area['id']?>"><i class="fas fa-edit"></a></td>
                <td><i role="button" class="fas fa-trash-alt btnEliminar" data-nombre="<?php echo $area['nombre'] ?>" data-id="<?php echo $area['id']?>"></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

  </div>
  <?php
$script ='<script type="text/javascript" src="../assets/areas.js"></script>';
echo $template->footer($script) ?>
<?php
include '../include/conexion.php';
include '../include/template.php';
include '../include/class/areas.php';
include '../include/class/empleados.php';

$areas =  new Areas();
$listadoAreas = $areas->listar();

$template =  new Template('Crear Area');

if(isset($_GET['id']) && is_numeric($_GET['id'])){
  $empleados =  new Empleados();
  $id_empleado = $_GET['id'];
  $empleado = $empleados->detalle($id_empleado);
  $titulo = "Editar Empleado";
}else{
  $titulo = "Crear Empleado";
  $id_empleado = 0;
}

?>
<?php echo $template->header() ?>
  <h1><?php echo $titulo ?></h1>
  <div class="row">
    <div class="col-md-12">
    <a href="index.php" class="btn btn-sm btn-primary float-end" ><i class="fas fa-undo"></i> Volver al Listado</a>
    </div>
    <div class="col-md-12">
      <form name="formEmpleado" id="formEmpleado">
        <div class="mb-3 row">
          <label for="nombre" class="col-sm-3 col-form-label fw-bold text-end">Nombre completo *</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" value="<?php echo ((isset($empleado['nombre']))?$empleado['nombre']:'') ?>" name="nombre"  required id="nombre" placeholder="Nombre completo del empleado">
          </div>
        </div>
        <div class="mb-3 row">
          <label for="email" class="col-sm-3 col-form-label fw-bold text-end">Correo electrónico *</label>
          <div class="col-sm-9">
            <input type="email" class="form-control"  value="<?php echo ((isset($empleado['email']))?$empleado['email']:'') ?>" name="email"   required id="email" placeholder="Correo electrónico">
          </div>
        </div>
        <div class="mb-3 row">
          <label for="sexo" class="col-sm-3 col-form-label fw-bold text-end">Sexo *</label>
          <div class="col-sm-9">
            <div class="form-check">
              <input class="form-check-input" type="radio" value="M" <?php echo ((isset($empleado['sexo']) && $empleado['sexo']=='M')?'checked':'') ?> name="sexo" id="sexo1">
              <label class="form-check-label" for="sexo1">
                Masculino
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" value="F" <?php echo ((isset($empleado['sexo']) && $empleado['sexo']=='F')?'checked':'') ?> name="sexo" id="sexo2">
              <label class="form-check-label" for="flexRadioDefault2">
                Femenino
              </label>
            </div>
          </div>
        </div>
        <div class="mb-3 row">
          <label for="correo" class="col-sm-3 col-form-label fw-bold text-end">Area *</label>
          <div class="col-sm-9">
            <select class="form-select" required="" name="area_id" aria-label="Default select example">
              <option value="" selected>** Seleccionar **</option>
              <?php foreach ($listadoAreas  as $key => $area): ?> 
              <option <?php echo ((isset($empleado['area_id']) && $area['id']==$empleado['area_id'])?'selected':'') ?> value="<?php echo $area['id'] ?>"><?php echo  $area['nombre'] ?></option>
              <?php endforeach ?>
            </select>
          </div>
        </div>
        <div class="mb-3 row">
          <label for="descripcion" class="col-sm-3 col-form-label fw-bold text-end">Descripción *</label>
          <div class="col-sm-9">
            <textarea name="descripcion" class="form-control"><?php echo ((isset($empleado['descripcion']))?$empleado['descripcion']:'') ?></textarea>
          </div>
        </div>
        <div class="mb-3 row">
          <div class="col-sm-9  offset-sm-3">
            <div class="form-check">
              <input class="form-check-input" <?php echo ((isset($empleado['boletin']) && $empleado['boletin']=='1')?'checked':'') ?> type="checkbox" value="1" id="boletin">
              <label class="form-check-label" for="boletin">
                Deseo recibir boletín informativo
              </label>
            </div>
          </div>
        </div>
        <div class="mb-3 row">
          <label for="descripcion" class="col-sm-3 col-form-label fw-bold text-end">Roles *</label>
          <div class="col-sm-9">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
              <label class="form-check-label" for="flexCheckDefault">
                Default checkbox
              </label>
            </div>
          </div>
        </div>
        <div class="mb-3 row">
          <div class="col-sm-9 offset-sm-3">
            <input type="hidden" name="case" value="crearEditarEmpleado">
            <input type="hidden" name="id" value="<?php echo $id_empleado ?>">
            <button class="btn btn-primary" id="BtnEnviar">Guardar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
<?php 
$script ='<script type="text/javascript" src="../assets/empleados.js"></script>';
echo $template->footer($script) ?>
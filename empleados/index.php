<?php
include '../include/conexion.php';
include '../include/template.php';
include '../include/class/empleados.php';
$template =  new Template('Listado de Empleados');
$empleados =  new Empleados();
$listadoEmpleados = $empleados->listar();

?>

<?php echo $template->header() ?>
  <h1>Lista de empleados</h1>
  <div class="row">
    <div class="col-md-12">
      <a href="form.php" class="btn btn-sm btn-primary float-end"><i class="fas fa-user-plus"></i> Crear</a>
    </div>
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th><i class="fas fa-user"></i> Nombre</th>
              <th><i class="fas fa-at"></i> Email</th>
              <th><i class="fas fa-venus-mars"></i> Sexo</th>
              <th><i class="fas fa-briefcase"> Area</th>
              <th><i class="fas fa-envelope"> Boletin</th>
              <th>Modificar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoEmpleados  as $key => $empleado): ?>
              <tr class="tr<?php echo $empleado['id']?>">
                <td><?php echo $empleado['nombre'] ?></td>
                <td><?php echo $empleado['email'] ?></td>
                <td><?php echo (($empleado['sexo']=='F')?'Femenino':'Masculino') ?></td>
                <td><?php echo $empleado['area'] ?></td>
                <td><?php echo (($empleado['boletin'])?'Si':'No') ?></td>
                <td><a href="form.php?id=<?php echo $empleado['id']?>"><i class="fas fa-edit"></a></td>
                <td><i role="button" class="fas fa-trash-alt btnEliminar" data-nombre="<?php echo $empleado['nombre'] ?>" data-id="<?php echo $empleado['id']?>"></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php
$script ='<script type="text/javascript" src="../assets/empleados.js"></script>';
echo $template->footer($script) ?>
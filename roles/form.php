<?php
include '../include/conexion.php';
include '../include/template.php';
include '../include/class/roles.php';
$template =  new Template('Crear Rol');

if(isset($_GET['id']) && is_numeric($_GET['id'])){
  $roles =  new Roles();
  $id_rol = $_GET['id'];
  $rol = $roles->detalle($id_rol);
  $titulo = "Editar Rol";
}else{
  $titulo = "Crear Rol";
  $id_rol = 0;
}

?>
<?php echo $template->header() ?>
  <h1><?php echo $titulo ?></h1>
  <div class="row">
    <div class="col-md-12">
      <a href="index.php" class="btn btn-sm btn-primary float-end" ><i class="fas fa-undo"></i> Volver al Listado</a>
    </div>
    <div class="col-md-12">
      <form name="formRol" id="formRol">
        <div class="mb-3 row">
          <label for="nombre" class="col-sm-3 col-form-label fw-bold text-end">Nombre *</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" value="<?php echo ((isset($rol['nombre']))?$rol['nombre']:'') ?>" name="nombre"  maxlength='255' id="nombre" required placeholder="Nombre del rol">
          </div>
        </div>
       
        <div class="mb-3 row">
          <div class="col-sm-9 offset-sm-3">
            <input type="hidden" name="case" value="crearEditarRol">
            <input type="hidden" name="id" value="<?php echo $id_rol ?>">
            <button class="btn btn-primary" id="BtnEnviar">Guardar</button>
          </div>
        </div>
    </form>
    </div>
  </div>
<?php 
$script ='<script type="text/javascript" src="../assets/roles.js"></script>';
echo $template->footer($script) ?>
<?php
include '../include/conexion.php';
include '../include/template.php';
include '../include/class/roles.php';
$template =  new Template('Listado de Roles');
$roles =  new Roles();
$listadoRoles = $roles->listar();

?>

<?php echo $template->header() ?>
  <h1>Lista de roles</h1>
  <div class="row">
    <div class="col-md-12">
      <a href="form.php" class="btn btn-sm btn-primary float-end"><i class="fas fa-user-plus"></i> Crear</a>
    </div>
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th><i class="fas fa-tags"></i> Nombre</th>
              <th>Modificar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($listadoRoles  as $key => $rol): ?>
              <tr class="tr<?php echo $rol['id']?>">
                <td><?php echo $rol['nombre'] ?></td>
                <td><a href="form.php?id=<?php echo $rol['id']?>"><i class="fas fa-edit"></a></td>
                <td><i role="button" class="fas fa-trash-alt btnEliminar" data-nombre="<?php echo $rol['nombre'] ?>" data-id="<?php echo $rol['id']?>"></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>

  </div>
  <?php
$script ='<script type="text/javascript" src="../assets/roles.js"></script>';
echo $template->footer($script) ?>
CREATE TABLE `areas` ( `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador del area' , `nombre` VARCHAR(255) NOT NULL COMMENT 'Nombre del area de la empresa' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `roles` ( `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador del rol' , `nombre` VARCHAR(255) NOT NULL COMMENT 'Nombre del rol' , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `empleados` ( `id` INT NOT NULL AUTO_INCREMENT , `nombre` VARCHAR(255) NOT NULL , `email` VARCHAR(255) NOT NULL , `sexo` CHAR(1) NOT NULL , `boletin` ENUM('0','1') NOT NULL DEFAULT '0' , `descripcion` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `empleados` ADD `area_id` INT NOT NULL AFTER `sexo`;
